/*! js-cookie v3.0.1 | MIT */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e=e||self,function(){var n=e.Cookies,o=e.Cookies=t();o.noConflict=function(){return e.Cookies=n,o}}())}(this,(function(){"use strict";function e(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var o in n)e[o]=n[o]}return e}return function t(n,o){function r(t,r,i){if("undefined"!=typeof document){"number"==typeof(i=e({},o,i)).expires&&(i.expires=new Date(Date.now()+864e5*i.expires)),i.expires&&(i.expires=i.expires.toUTCString()),t=encodeURIComponent(t).replace(/%(2[346B]|5E|60|7C)/g,decodeURIComponent).replace(/[()]/g,escape);var c="";for(var u in i)i[u]&&(c+="; "+u,!0!==i[u]&&(c+="="+i[u].split(";")[0]));return document.cookie=t+"="+n.write(r,t)+c}}return Object.create({set:r,get:function(e){if("undefined"!=typeof document&&(!arguments.length||e)){for(var t=document.cookie?document.cookie.split("; "):[],o={},r=0;r<t.length;r++){var i=t[r].split("="),c=i.slice(1).join("=");try{var u=decodeURIComponent(i[0]);if(o[u]=n.read(c,u),e===u)break}catch(e){}}return e?o[e]:o}},remove:function(t,n){r(t,"",e({},n,{expires:-1}))},withAttributes:function(n){return t(this.converter,e({},this.attributes,n))},withConverter:function(n){return t(e({},this.converter,n),this.attributes)}},{attributes:{value:Object.freeze(o)},converter:{value:Object.freeze(n)}})}({read:function(e){return'"'===e[0]&&(e=e.slice(1,-1)),e.replace(/(%[\dA-F]{2})+/gi,decodeURIComponent)},write:function(e){return encodeURIComponent(e).replace(/%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g,decodeURIComponent)}},{path:"/"})}));
const search = function (search_form) {

    const _this = this;

    this.search_box = search_form.find("input[type=search]");
    this.suggestion_box = $("<div>", {id: "suggestion-box"}).hide();

    this.show_suggestions = function (search_term) {
        window.Lifeboat.Search.search(search_term, 5, function (results) {
            let suggestions = [];

            for (let i = 0; i < results.length; i++) {
                let html = [_this.highlight_term(results[i].term, search_term)];

                if (results[i].hasOwnProperty('collection') && results[i].collection) {
                    html.push(' <i>in ' + results[i].collection +'</i>')
                }

                suggestions.push(
                    $("<a>", {href: results[i].url}).html(html)
                        .click(function () {
                            window.location = results[i].url;
                        })
                );
            }

            if (suggestions.length > 0) {
                _this.suggestion_box.html(suggestions).show();
            } else {
                _this.suggestion_box.hide();
            }
        });
    };

    this.highlight_term = function (string, term) {
        let start = string.toLowerCase().indexOf(term.toLowerCase());
        let end = start + term.length + 3; // 3 is the the length of '<b>'

        string = string.substring(0, start) + '<b>' + string.substring(start);
        return string.substring(0, end) + '</b>' + string.substring(end);
    }

    this.search_box.keyup(function (e) {
        let term = $(this).val();
        if (term.length > 1) _this.show_suggestions(term);
    });

    $(window).click(function () {
        _this.suggestion_box.hide();
    });

    search_form.append(this.suggestion_box);

    // Update search location
    search_form.find('select').on('change update', function () {
        if ($(this).is(":visible")) {
            search_form.attr('action', $(this).find(':selected').data('url'));
        } else {
            search_form.attr('action', '/search');
        }
    }).change();
};

const product_gallery = function (gallery) {

    const _this         = this;
    const images        = gallery.find('.images');
    const thumbnails    = gallery.find('.thumbnails');
    const lightbox      = gallery.find('.lightbox');
    const light_close   = lightbox.find('.close-btn');
    const light_image   = lightbox.find('.lightbox-image');
    const light_thumbs  = lightbox.find('.lightbox-thumbnails');
    
    images.find("picture").each(function (index) {
        let $this = $(this);

        this.setAttribute('data-index', index);

        thumbnails.append($this.clone());
        light_thumbs.append($this.clone());
    });

    thumbnails.on('click', 'picture', function (e) {
        e.preventDefault();
        _this.set_active($(this).data('index'), true);
    });

    light_thumbs.on('click', 'picture', function (e) {
        e.preventDefault();
        _this.set_active($(this).data('index'), true);
    });

    images.click(function (e) {
        e.preventDefault();
        lightbox.addClass('show');
    });

    _this.set_active = function (index, move = false) {
        thumbnails.find('picture').removeClass('active');
        thumbnails.find('picture[data-index=' + index + ']').addClass('active');

        light_thumbs.find('picture').removeClass('active');
        light_thumbs.find('picture[data-index=' + index + ']').addClass('active');

        light_image.html($(images.find('picture[data-index=' + index + ']')[0]).clone());

        if (move) carousel.trigger('to.owl.carousel', [index, 300]);
    };

    _this.update_thumbnails = function () {
        setTimeout(function () {
            let index = images.find('.owl-item.active picture').data('index');
            _this.set_active(index, false);
        }, 100);
    };

    // init
    const carousel = images.owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: true,
        margin: 0,
        mergeFit: true,
        autoHeight: true,
        singleItem: true,
        dots: false,
        navigation: false,
        //onRefreshed: () => { thumbnails.height(images.find('.owl-stage-outer').height()); },
        onChanged: () => { _this.update_thumbnails(); }
    }).trigger('refresh.owl.carousel');

    light_close.click(function (e) {
        e.preventDefault();
        lightbox.removeClass('show');
    });

    lightbox.click(function (e) {
        if (e.target === lightbox[0]) lightbox.removeClass('show');
    });

    light_thumbs.find('picture:first-child').click();
};

const price_form = function (form) {

    let _this = this;

    this.info_panel         = $("#" + form.data('panel'));
    this.stock_availability = this.info_panel.find('.stock-availability');
    this.add_cart_btn       = this.info_panel.find('.add-cart-btn');
    this.show_now_btn       = this.info_panel.find('.buy-now-btn');
    this.info_price         = this.info_panel.find('.price');
    this.info_text          = this.info_panel.find('.text');
    this.quantity_field      = this.info_panel.find('#quantity');
    this.delivery_info      = this.info_panel.find('.delivery-info');
    this.trust_info         = this.info_panel.find('.trust-info');
    this.price_container    = $("<div>", {class: 'price row nw'});
    this.variants_container = $("<div>", {class: 'variants'});

    this.make_select_field = function (option, opts) {
        let elem = $("<select>", {'id': option, 'name': option});

        opts.forEach(function (item) {
            elem.append($("<option>", {'value': item}).html(item));
        });

        return elem;
    };

    this.make_select = function (option, name, options) {
        let select_options = [];

        options.forEach(function (item) {
            select_options.push($("<option>", {'value': item}).html(item));
        });

        return $("<div>", {'class': "variant row nw"}).html([
            $("<label>", {'for': option}).html(name + ":"),
            $("<div>", {'class': 'select-box'}).html(
                _this.make_select_field(option, options)
            )
        ])
    };

    this.render_price = function () {
        let elems = '';

        Lifeboat.Product.CalculatePrice(form.data("id"), _this.get_variant_data(), 1, function (data) {
            elems = ["<ins>" + data.SellingPrice + "</ins>"];
            _this.info_price.html(elems);

            _this.stock_availability.html('');
            _this.quantity_field.prop('max', '');

            if (data.isDiscounted) elems.push('<del>' + data.BasePrice + '</del>');

            if (data.StockAmount === 0) {
                _this.info_text.html($("<span>", {class: 'out-of-stock'}).html([
                    '<strong>Out of Stock</strong>',
                    '<br /><small>This product is currently out of stock.</small>'
                ]));
                _this.show_buttons(false);
            } else if (data.StockAmount < 0) {
                _this.info_text.html($("<span>", {class: 'out-of-stock'}).html([
                    '<strong>Currently Unavailable</strong>',
                    '<br /><small>This product is currently unavailable</small>'
                ]));
                _this.show_buttons(false);
            } else if (data.StockAmount === 10000) {
                _this.info_text.html($("<span>", {class: 'in-stock'}).html([
                    '<strong>Available</strong>'
                ]));
                _this.show_buttons(true);
            } else {
                _this.info_text.html($("<strong>", {class: 'in-stock'}).html('In Stock'));
                _this.show_buttons(true);
                
                if (data.StockAmount < 3) {
                    _this.stock_availability.html('<small>Last ' + data.StockAmount + ' available</small>');
                }
                 _this.quantity_field.prop('max', data.StockAmount);
            }

            _this.price_container.html(elems);
        });
    };
    
    this.show_buttons = function (show) {
        if (show) {
            this.add_cart_btn.show();
            this.show_now_btn.show();
            this.quantity_field.parent('.row').show();
            this.delivery_info.show();
            this.trust_info.show();
        } else {
            this.add_cart_btn.hide();
            this.show_now_btn.hide();
            this.quantity_field.parent('.row').hide();
            this.delivery_info.hide();
            this.trust_info.hide();
        }
    }

    this.render_variants = function () {
        Lifeboat.Product.Variants(form.data("id"), function (data) {
            _this.variants_container.html("");

            if (typeof data === 'object' && data !== null) {
                for (let x = 1; x < 4; x++) {
                    let opt_name = 'Option' + x;
                    if (data[opt_name]) {
                        _this.variants_container.append(
                            _this.make_select('Option' + x, data[opt_name].Name, Object.keys(data[opt_name].Options))
                        );
                    }

                    _this.variants_container.find("select#" + opt_name).change(function () {
                        let $this = $(this),
                            elem_id = $this.attr('id'),

                            val = $this.val();

                        for (let y = 1; y < 4; y++) {
                            let check_opt = 'Option' + y;
                            if (elem_id === check_opt) continue;

                            let select  = _this.variants_container.find("select#" + check_opt);
                            let allow   = false;
                            let curr    = select.val();

                            data[elem_id].Options[val][check_opt].forEach(function (item) {
                                if (item === curr) allow = true;
                            });

                            if (!allow) {
                                select.val(data[elem_id].Options[val][check_opt][0]);
                            }
                        }
                        _this.render_price();
                    });
                }

                $(_this.variants_container.find("select")[0]).trigger("change");
            }

            _this.render_price();
        });
    };

    this.get_variant_data = function () {
        return {
            "Option1": form.find("#Option1").val(),
            "Option2": form.find("#Option2").val(),
            "Option3": form.find("#Option3").val(),
        };
    };

    form.append([
        this.price_container,
        this.variants_container
    ]);

    _this.render_variants();

    this.add_cart_btn.click(function (e) {
        e.preventDefault();
        Lifeboat.Cart.AddItem(
            form.data("id"),
            _this.get_variant_data(),
            _this.quantity_field.val()
        );
    });

    this.show_now_btn.click(function (e) {
        e.preventDefault();
        window.history.pushState('', '', '/cart');
        _this.add_cart_btn.click();
    });
};

const carousel = function (carousel) {

    let slides  = parseInt(carousel.data('slides'));
    let small   = parseInt(carousel.data('small')) ? parseInt(carousel.data('small')) : slides;
    let medium  = parseInt(carousel.data('medium')) ? parseInt(carousel.data('medium')) : small;
    let large   = parseInt(carousel.data('large')) ? parseInt(carousel.data('large')) : medium;

    carousel.owlCarousel({
        stagePadding: 0,
        items: slides,
        loop: false,
        margin: 0,
        mergeFit: true,
        autoHeight: true,
        dots: false,
        nav: carousel.data('nav') ? true : false,
        navText: ["<i class='icon-chevron-left'></i>","<i class='icon-chevron-right'></i>"],
        responsive : {
            0: {items: slides},
            452: {items: small},
            768: {items: medium},
            1024: {items: large}
        }
    });
};

const tabbed_content = function (controls, target) {

    controls.find('a').click(function (e) {
       const $this = $(this);
       const _href = $this.attr('href');
       const t_elem = (_href.indexOf('#') === 0) ? target.find('.tabbed-content'+_href) : null;

       if (t_elem !== null) {
           e.preventDefault();
           target.find('.tabbed-content').removeClass('show');
           t_elem.addClass('show');

           controls.find('a').removeClass('active');
           $this.addClass('active');
       }
    });

    controls.find('a.active').click();
};

const PriceRangeFilter = function (parent) {
    const min = parent.data('min');
    const max = parent.data('max');

    const selected_min = parent.data('selected-min');
    const selected_max = parent.data('selected-max');

    const param_min = 'price_min';
    const param_max = 'price_max';

    const filter_items = parent.find('ul.filter-items');

    let url       = new URL(window.location.href);
    let params    = new URLSearchParams(url.search);

    params.delete(param_min);
    params.delete(param_max);

    url.search = params.toString();

    this.create_link = function (val, param) {
        let _params = params;
        _params.set(param, val);

        let _url = url;
        _url.search = _params.toString();

        return {
            'label': (param === param_min) ? 'more than €' + val : 'up to €' + val,
            'value': url.toString()
        };
    };

    if (selected_min > 0) {
        filter_items.append([
            $("<li>").html('<strong>Showing items more than €' + selected_min + '</strong>').css('padding-bottom', '1rem'),
            $("<li>").html($("<a>", {href: url.toString(), target: '_self', rel: 'noopener nofollow', class: 'clear'}).html('Clear filter'))
        ]);
        parent.append($("<input>", {type: 'hidden', class: 'hide', name: param_min, value: selected_min}));
        parent.find('h3').click();
    } else if (selected_max < 501) {
        filter_items.append([
            $("<li>").html('<strong>Showing items up to €' + selected_max + '</strong>').css('padding-bottom', '1rem'),
            $("<li>").html($("<a>", {href: url.toString(), target: '_self', rel: 'noopener nofollow', class: 'clear'}).html('Clear filter'))
        ]);
        parent.append($("<input>", {type: 'hidden', class: 'hide', name: param_max, value: selected_max}));
        parent.find('h3').click();
    } else {
        let options = [];

        if (min < 10 && max > 9) options.push(this.create_link(10, param_max));
        if (min < 20 && max > 19) options.push(this.create_link(20, param_max));
        if (min < 30 && max > 29) options.push(this.create_link(30, param_max));
        if (min < 50 && max > 49) options.push(this.create_link(50, param_max));
        if (min < 100 && max > 99) options.push(this.create_link(100, param_max));
        if (min < 250 && max > 249) options.push(this.create_link(250, param_max));
        if (min < 500 && max > 499) options.push(this.create_link(500, param_max));

        if (max > 49) options.push(this.create_link(50, param_min));
        if (max > 99) options.push(this.create_link(100, param_min));
        if (max > 249) options.push(this.create_link(250, param_min));
        if (max > 499) options.push(this.create_link(500, param_min));

        options.forEach(function (opt) {
            filter_items.append(
                $("<li>").html($("<a>", {href: opt.value, target: '_self', rel: 'noopener nofollow'}).html(opt.label))
            );
        });

        if (options.length < 3) parent.hide();
    }
};

// Init page load
(function ($){
    const _search = new search($("#search-form"));

    // Main Menu Mobile
    const sidebar = $("#sidebar");
    const sidebar_toggle = $(".sidebar-toggle");

    sidebar_toggle.click(function (e) {
        e.preventDefault();
        $($(this).data('target')).toggleClass('open')
    });

    $('.tab-controls').each(function () {
        new tabbed_content($(this), $($(this).data('target')));
    });

    const submenu_buttons = sidebar.find(".has-submenu");
    submenu_buttons.click(function (e) {
        e.preventDefault();
        sidebar.addClass('sub-open');
        sidebar.find('#' + $(this).data('target')).addClass('show');
    });

    const sub_close_button = sidebar.find(".sub-back-btn");
    sub_close_button.click(function (e) {
        e.preventDefault();
        sidebar.removeClass('sub-open');
        sidebar.find('.sub-category').removeClass('show');
    });

    const tooltips = $(".tooltip-hover");
    tooltips.on('mouseover click', function () {
        let $this = $(this);
        $this.append($("<div>", {class: 'tooltip-text'}).html($this.data('tooltip')));
    }).on('mouseleave mouseout', function () {
        $(this).find('.tooltip-text').remove();
    });

    $("#price-form").each(function (){ new price_form($(this)); });
    $(".product-top .gallery").each(function () { new product_gallery($(this)); });
    $(".carousel.owl-carousel").each(function () { new carousel($(this)); });

    $(".search-filter h3").click(function () {
        $(this).parents('.search-filter').toggleClass('open');
    });

    $(".filter-items input").change(function () {
        $(this).parents('form').submit();
    });

    $(".price ins").each(function () {
        let txt     = $(this).text();
        let split   = txt.split('.');

        $(this).html([
            '<span class="sign">' + txt[0] + '</span>',
            '<span class="amt">' + split[0].replace(txt[0], '') + '</span>',
            '<span class="cents">' + split[1] + '</span>'
        ]);
    });

    $(".cart-item .quantity").blur(function (e) {
        let parent  = $(this).parents('.cart-item');

        Lifeboat.Cart.UpdateQuantity(
            parent.data('id'),
            parent.data('variant'),
            $(this).val()
        );
    });

    $(".cart-item .rfc").click(function (e) {
        let parent  = $(this).parents('.cart-item');

        Lifeboat.Cart.RemoveItem(parent.data('id'), parent.data('variant'));
    });

    $("#secure-btn").click(function (e) {
        e.preventDefault();
        $("#secure-payments").css('display', 'block !important').addClass('show');
    });

    $("#secure-payments .close-btn").click(function (e) {
        e.preventDefault();
        $("#secure-payments").removeClass('show');
    });

    $("#return-btn").click(function (e) {
        e.preventDefault();
        $("#return-policy").css('display', 'block !important').addClass('show');
    });

    $("#return-policy .close-btn").click(function (e) {
        e.preventDefault();
        $("#return-policy").removeClass('show');
    });
    
    $("select.filter-list").change(function () {
        window.location = $(this).val();
    });

    $(".search-filter.price-range").each(function () {
        new PriceRangeFilter($(this));
    });

    const age_verify = $("#age-verify");
    if (age_verify.length > 0) {
        if (parseInt(Cookies.get('age_verify')) !== 1) {
            age_verify.addClass('show');
            age_verify.find('.close.btn').click(function (e) {
                e.preventDefault();
                Cookies.set('age_verify', 1);
                age_verify.removeClass('show');
            });
        }
    }
})(jQuery);