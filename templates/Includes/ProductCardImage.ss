<a href="$Link">
    <picture>
        <source srcset="$Image.Pad(640,640).AbsoluteURL" media="(min-width: 640px) or (-webkit-min-device-pixel-ratio: 2)" />
        <img src="$Image.Pad(320,320).AbsoluteURL" alt="$Title" width="320" height="320" loading="lazy" />
    </picture>
</a>