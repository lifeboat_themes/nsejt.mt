<% if $ObjectsByTag('Collection', 'brand_page').find('Title', $Vendor) %>
    <a href="$ObjectsByTag('Collection', 'brand_page').find('Title', $Vendor).Link" title="Visit {$Vendor}'s store" class="brand-link">$Vendor</a>
<% else %>
    <a href="/search?search=$Vendor" class="brand-link" title="Search for more product by $Vendor">$Vendor</a>
<% end_if %>