<div class="filter-actions row">
    <button type="button" class="filter-btn sidebar-toggle" data-target=".search-sidebar">Filter</button>
    <% if $getSearchTerm() %>
        <div class="search-term">
            <small>Searching for:</small>
            <strong>$getSearchTerm()</strong>
        </div>
    <% end_if %>
    <select name="sort" class="filter-list">
        <% loop $getSortOptions() %>
            <option value="$FilterLink" <% if $Selected %>selected="selected"<% end_if %>>
                $Option
            </option>
        <% end_loop %>
    </select>
</div>

<div class="row responsive">
    <% include SearchSidebar %>
    <section class="c10 search-results">
        <div class="results">
            <% if $PaginatedResults(16).count > 0 %>
            <% loop $PaginatedResults(16) %>
                <% include ProductCardRow %>
            <% end_loop %>
            <% else %>
                <div class="no-results">
                    <h2>No Results Found</h2>
                    <p>We could not find any product that matches your search.</p>

                    <h3>Things to try...</h3>
                    <ul>
                        <li>Modify your search to include; brand, size, name, or other aspects of the product you're searching for.</li>
                        <li>Start a new search</li>
                        <li>Clear the search filters</li>
                    </ul>
                </div>
            <% end_if %>
        </div>
        <div class="pagination">
            <% if $PaginatedResults(16).MoreThanOnePage %>
                <% with $PaginatedResults(16) %>
                    <ul>
                        <% if $NotFirstPage %>
                            <li>
                                <a class="prev" href="$PrevLink" aria-label="Previous" tabindex="-1"
                                   aria-disabled="true">
                                    Prev
                                </a>
                            </li>
                        <% end_if %>
                        <% loop $PaginationSummary(2) %>
                            <% if $CurrentBool %>
                            <li><a href="#" class="active">$PageNum</a>
                            <% else %>
                                <% if $Link %>
                                    <li><a href="$Link">$PageNum</a></li>
                                <% else %>
                                    <li>...</li>
                                <% end_if %>
                            <% end_if %>
                        <% end_loop %>
                        <% if $NotLastPage %>
                            <li>
                                <a class="next" href="$NextLink" aria-label="Next">
                                    Next
                                </a>
                            </li>
                        <% end_if %>
                    </ul>
                <% end_with %>
            <% end_if %>
        </div>
    </section>
</div>