<div class="container">
    <div class="row">
        <div class="page-not-found">
            <h1>404</h1>
            <h2>Page Not Found</h2>
            <p>It looks like this page no longer exists.</p>
            <a href="/" class="btn">Go Home</a>
        </div>
    </div>
</div>